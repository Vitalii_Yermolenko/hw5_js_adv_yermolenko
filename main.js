const blockUsers = document.querySelector('.users-block');
const blockPosts = document.querySelector('.posts-block');
const createPost = document.querySelector('.block-create-post__button');

const loader = document.createElement('div');
loader.innerHTML = '<i class="fa fa-spinner fa-4x fa-spin"></i>';
blockPosts.append(loader);


class Card {
    constructor(name, userId, email, title, text, postId){
        this._name = name;
        this._userId = userId;
        this._email = email;
        this._title = title;
        this._text = text;
        this._postId = postId;
    }

    get name(){
        return this._name;
    }

    get userId(){
        return this._userId;
    }

    get email(){
        return this._email;
    }

    get title(){
        return this._title;
    }

    get text(){
        return this._text;
    }

    get postId(){
        return this._postId;
    }

    createCard(){
        const div = document.createElement('div');
        const blockName = document.createElement('h2');
        const blockEmail = document.createElement('h3');
        const blockTitle = document.createElement('p');
        const blockText = document.createElement('p');
        div.dataset.postId = this._postId;
        div.dataset.userId = this._userId;
        blockName.innerText = this._name;
        blockEmail.innerText = this._email;
        blockTitle.innerText = this._title;
        blockText.innerText = this._text;

        div.classList.add('post-block');
        blockName.classList.add('post-block__name');
        blockEmail.classList.add('post-block__email');
        blockTitle.classList.add('post-block__title');
        blockText.classList.add('post-block__text');

        div.append(blockName);
        div.append(blockEmail);
        div.append(blockTitle);
        div.append(blockText);

        // const buttonEdit = document.createElement('button');
        // buttonEdit.innerText = "Edit";
        // buttonEdit.classList.add('post-block__edit-button');
        // buttonEdit.addEventListener('click', () => {
        //     console.log(blockTitle.offsetWidth);
        //     const inputTitle = document.createElement('input');
        //     const inputText = document.createElement('input');
        //     inputTitle.classList.add('post-block__title-edit')
        //     inputText.classList.add('post-block__text-edit')
        //     inputTitle.value = blockTitle.innerText;
        //     inputText.value = blockText.innerText;
        //     div.append(inputTitle);
        //     div.append(inputText);
        // });
        // div.append(buttonEdit);

        const buttonClose = document.createElement('button');
        buttonClose.innerText = "X";
        buttonClose.classList.add('post-block__close-button');
        buttonClose.addEventListener('click', () => {

            fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
                method: 'DELETE',
                headers: {
                    'Content-type': 'application/json; charset=UTF-8'
                   }
              }).then((response) => {
                    if(response){
                        div.remove();
                    }
                })
        });

        div.append(buttonClose);
        blockPosts.prepend(div);
        return;
    }
}


function getRequest(url) {
    return fetch(url, {
        method: `GET`
}).then((response) =>{
    return response.json();
    })
}


function infoUser(user,arrPost) {
    const {id,name, email} = user;
    arrPost.map(post =>{
        const {id:postId,userId,title, body} = post;
        if (id === userId) {
            let userCard = new Card( name, userId, email, title, body, postId);
            loader.remove();
            userCard.createCard();
        }

    })

}


Promise.all([getRequest('https://ajax.test-danit.com/api/json/users'),getRequest('https://ajax.test-danit.com/api/json/posts')])
.then((result) => {
    result[0].map(user => {infoUser(user,result[1])});  
})
.catch(error => console.log(error));

createPost.addEventListener('click', () => {
    const form = document.createElement('div');
    form.classList.add('block-input-data');
    const inputTitleCreate = document.createElement('input');
    const labelTitleCreate = document.createElement('label');
    const inputTextCreate = document.createElement('textarea');
    const inputSubmitCreate = document.createElement('button');
    labelTitleCreate.innerText = 'Title ';
    labelTitleCreate.classList.add('block-input-data__title');
    inputTitleCreate.placeholder = 'Title post';
    inputTitleCreate.classList.add('block-input-data__title-input');
    inputTitleCreate.type = 'text';
    labelTitleCreate.append(inputTitleCreate);
    inputTextCreate.classList.add('block-input-data__text-input');
    inputTextCreate.placeholder = 'text post';
    inputTextCreate.name = 'inputText';
    inputTextCreate.rows = 10;
    inputSubmitCreate.classList.add('block-input-data__submit');
    inputSubmitCreate.innerText = 'Submit';

    form.append(labelTitleCreate);
    form.append(inputTextCreate);
    form.append(inputSubmitCreate);

    document.body.append(form);
    inputSubmitCreate.addEventListener('click', (e) => {
        e.defaultPrevented;
        let inputDataTitle = inputTitleCreate.value;
        let inputDataText = inputTextCreate.value;
        console.log(inputDataText);
    if(inputDataTitle && inputDataText){
        userId = +blockPosts.firstChild.dataset.userId;
        name = 'Leanne Graham';
        email = 'Sincere@april.biz';
        title = inputDataTitle;
        body = inputDataText;
        console.log(blockPosts.firstChild.dataset.postId);
        postId = +blockPosts.firstChild.dataset.postId + 1;
        let userCard = new Card( name, userId, email, title, body, postId);
        userCard.createCard();
        fetch('https://ajax.test-danit.com/api/json/posts',{
            method:"POST",
            body: JSON.stringify({
                id:userCard.postId,
                userId : userCard.userId,
                title : userCard.title,
                body : userCard.text,
              }),
              headers: {
                'Content-Type': 'application/json'
              }
            })
            .then(response => response.json())
        form.remove();
    } else{
        alert('input data in form');
    }

    })


})